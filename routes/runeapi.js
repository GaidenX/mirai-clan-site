const rsapi = require('runescape-api');

function logInfo(info) {
    console.log(info);
}

// function logInfoClan(data) {
//     let clanList = []

//     Object.keys(data).map(function (key, index) {
//         if (data[key].rank === 'Owner') {
//             clanList.push(data[key]);
//         }
//     });
//     console.log('clanList');
//     return clanList;
// }

exports.getClanMembers = function (req, res) {
    return new Promise((resolve, reject) => {
        rsapi.rs.hiscores.clan('Mirai Nikki').then(data => {
            let clanList = []

            Object.keys(data).map(function (key, index) {
                if (data[key].rank === 'Owner' || data[key].rank === 'Coordinator' || data[key].rank === 'Admin') {
                    clanList.push(data[key]);
                }
            });
            resolve(clanList);
        })
    });
}


// rsapi.rs.bestiary.beast(49).then(function(beast) {
//     console.log(JSON.stringify(beast, null, 2));
// }).catch(console.error);

// rsapi.rs.hiscores.player('vandulando').then(logInfo).catch(console.error);
// rsapi.rs.bestiary.beastsByTerms('solak').then(function(beasts) {
//     console.log(beasts);
// }).catch(console.error);

// rsapi.rs.bestiary.beast(25513).then(function(beast) {
//     console.log(beast);
// }).catch(console.error);