const express = require('express');
const cookieParser = require('cookie-parser');
const app = express().use(function (req, res, next) {
    if (req.header('x-forwarded-proto') == 'http') {
        res.redirect(301, 'https://' + 'mirainikki.herokuapp.com' + req.url)
        return
    }
    next()
});
const server = require('https').createServer(app);
const ENV = require('dotenv');
ENV.config();
const porta = process.env.PORT || 3000;

// Other modules
const runeApi = require('./routes/runeapi');
//---

// General Setup
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({ // to support URL-encoded bodies
    extended: true
}));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
//---

// HandleBars - Template Engine
const handleBars = require('express-handlebars');
const path = require('path');

app.set('views', path.join(__dirname, '/views'));
app.engine('handlebars', handleBars({
    defaultLayout: "index",
    helpers: {
        is: function (v1, operator, v2, options) {
            switch (operator) {
                case '==':
                    return (v1 == v2) ? options.fn(this) : options.inverse(this);
                case '===':
                    return (v1 === v2) ? options.fn(this) : options.inverse(this);
                case '!=':
                    return (v1 != v2) ? options.fn(this) : options.inverse(this);
                case '!==':
                    return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                case '<':
                    return (v1 < v2) ? options.fn(this) : options.inverse(this);
                case '<=':
                    return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                case '>':
                    return (v1 > v2) ? options.fn(this) : options.inverse(this);
                case '>=':
                    return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                case '&&':
                    return (v1 && v2) ? options.fn(this) : options.inverse(this);
                case '||':
                    return (v1 || v2) ? options.fn(this) : options.inverse(this);
                default:
                    return options.inverse(this);
            }
        }
    },
}));
app.set('view engine', 'handlebars');
//---

/** Pages Routes Website **/
app.get('/home', function (request, response) {
    response.render('home', {
        homepage: true,
        style: ['/css/main.css', '/css/home.css'],
        javascript: []
    });
});
/** END Pages Routes **/

/** API Routes **/
app.get('/listclan', function (request, response) {
    runeApi.getClanMembers().then(membersData => {
        response.render('members', {
            homepage: false,
            membersList: membersData,
            style: ['/css/main.css', '/css/home.css'],
            javascript: []
        });
    }).catch(err => {
        console.log(err);
        //Error page
        response.render('members', {
            homepage: false,
            style: ['/css/main.css', '/css/home.css'],
            javascript: []
        });
    });
});
/** END API Routes **/

//All Routes and 404 page for now only render HomePage
app.get('*', function (request, response) {
    response.render('home', {
        homepage: true,
        style: ['/css/main.css', '/css/home.css'],
        javascript: []
    });
});

app.listen(porta, function () {
    console.log("server on in: " + porta)
});